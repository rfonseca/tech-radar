---
title: "{{ replace .Name "-" " " | title }}"
date: {{ .Date }}
draft: true

# Pick one: languages | platforms | techniques | tools
quadrant: "languages"
# Pick one ring: adopt | trial | assess | hold
ring: "adopt"
# Pick one moved: in | no | out
moved: "no"
---

Notes:
- Pick one for each quadrant, ring and moved above.
- Remember to write:
  - What is it?
  - Why?
  - Links and references

