---
title: "Emacs"
date: 2023-04-05T10:18:15-03:00

quadrant: "tools"
ring: "adopt"
moved: "no"
---

# What is it?

[Emacs](https://www.gnu.org/software/emacs/) is a very extensible text editor. It can be customized to be anything you like. Its power lies if the fact that if has an embed lisp interpreter and all aspects of the editor can be altered by it. Both visuals and functions.

This is so powerful that nowadays there are Emacs plugins that can make Emacs be used as a text editor, IDE, music player, web browser, RSS feed, e-mail client and much more.

Personally, I think that the default Emacs experience is problematic. The default keybindings are unique to it and you have to rely on a lot of `ctrl+alt+shift` keybindings. But with the [doom](https://github.com/doomemacs/doomemacs) configuration framework using Emacs became very easy to use and extend with new plugins. It brings vim like keybindings and a plugin system with a very large repository of pre-configured plugins.

There are various configuration plugins for Emacs. Another one that is very interesting is [Spaceemacs](https://www.spacemacs.org/) but I ended up preferring doom.

References:

- https://www.gnu.org/software/emacs/
- https://github.com/doomemacs/doomemacs
- https://www.spacemacs.org/
