---
title: "Go"
date: 2023-04-04T08:43:49-03:00

quadrant: "languages"
ring: "adopt"
moved: "no"
---

Language created by Google developers but it was open sourced.

Its language is very simple and easy to lean. 

Because the language is very simple, it can be verbose, but this makes easy for onboarding new people and also to read code from others.

Its compiled and its compilation is very fast.

Go is garbage collected and has a very efficient memory model.

References:

- https://go.dev/

